﻿using System.Threading.Tasks;
using System;
using System.Threading;
using TechService.ConsoleApp.Models;
using TechService.ConsoleApp.Services;

namespace TechService.ConsoleApp.Domain
{
    public class Worker
    {
        private const short maxPersent = 100;
        private const short delay = 100;
        private const short onePersent = 1;
        private readonly WorkItemExcelService excelService;

        public int Id { get; private set; }

        public WorkItemContract CurrentWorkItem { get; set; }

        public bool IsReadyForWork { get; set; } = true;

        public Worker(int id, WorkItemExcelService workItemExcelService)
        {
            Id = id;
            excelService = workItemExcelService;
        }

        public async Task ProcessWorkItem(WorkItemContract workItem)
        {
            workItem.WorkItem.IsInProcess = true;
            IsReadyForWork = false;
          //  CurrentWorkItem = workItem;

            Console.WriteLine($"Worker with Id {Id} started processing task with Id {workItem.WorkItem.Id} and Priority {workItem.WorkItem.PriorityType}");

            while(workItem.WorkItem.CompletedPersent < maxPersent)
            {
                if (workItem.CancellationToken.IsCancellationRequested)
                {
                    Console.WriteLine("Canceled");
                    workItem.WorkItem.IsInProcess = false;
                   
                   // IsReadyForWork = true;

                    workItem.CancellationToken.ThrowIfCancellationRequested();
                }
                await Task.Delay(delay);
                workItem.WorkItem.CompletedPersent += onePersent;
            }

            if (workItem.WorkItem.CompletedPersent == maxPersent)
            {
                Console.WriteLine($"Worker with Id {Id} finished processing task with Id {workItem.WorkItem.Id} and Priority {workItem.WorkItem.PriorityType}");
              //  workItem.WorkItem.IsInProcess = false;
              //  workItem.WorkItem.IsCompleted = true;
            }

            if (workItem.WorkItem.WorkItemType == WorkItemType.BuyingActivities)
            {
                RemoveBuyingActivityItem(workItem.WorkItem.RowIndex);
            }
            workItem.WorkItem.IsInProcess = false;
            IsReadyForWork = true;
        }

        public void RemoveBuyingActivityItem(int workItemRowNumber)
        {
            excelService.RemoveWorkItem(Id, workItemRowNumber);
        }

    }
}
