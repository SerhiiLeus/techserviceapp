﻿using System.Threading;
using TechService.ConsoleApp.Domain;

namespace TechService.ConsoleApp.Models
{
    public class WorkItemContract
    {
        public CancellationTokenSource CancellationTokenSource { get; set; }

        public CancellationToken CancellationToken { get; set; }

        public WorkItem WorkItem { get; set; }

        private WorkItemContract()
        {
            CancellationTokenSource = new CancellationTokenSource();
            CancellationToken = CancellationTokenSource.Token;
        }

        public WorkItemContract(WorkItem workItem)
            : this()
        {
            WorkItem = workItem;
        }
    }
}
