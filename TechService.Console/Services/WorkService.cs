﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TechService.ConsoleApp.Domain;
using TechService.ConsoleApp.Models;

namespace TechService.ConsoleApp.Services
{
    public class WorkService
    {
        private const int completedItemPersent = 100;
        private readonly WorkItemExcelService excelService;
        private List<WorkItemContract> workItems = new List<WorkItemContract>();
        private List<WorkItemContract> notCompletedWorkItems = new List<WorkItemContract>();
        private List<Worker> workers = new List<Worker>();
        private int workersCount = 1;

        public WorkService(int workers)
        {
            workersCount = workers;
            excelService = new WorkItemExcelService();

            InitWorkers();
        }

        public void AddWorkItems(IEnumerable<WorkItem> items)
        {
            var workitemsContracts = items.Select(item => new WorkItemContract(item));
            workItems = workItems.Concat(workitemsContracts).ToList();
        }

        public async Task Work()
        {
            var buiyingActivitiesItems = excelService.GetWorkItemsFormExcel();
            workItems.AddRange(buiyingActivitiesItems.Select(x => new WorkItemContract(x)));

            while (true)
            {               
                await Task.Delay(1000);
                ProcessWorkItems(GetOrderedWorkItems());
            }
        }

        private IEnumerable<WorkItemContract> GetOrderedWorkItems()
        {
            var items = new List<WorkItemContract>();

            items.AddRange(workItems.Where(x => x != null && !x.WorkItem.IsInProcess && 
                (x.WorkItem.PriorityType == PriorityType.High || x.WorkItem.PriorityType == PriorityType.Medium))?.OrderByDescending(x => x.WorkItem.PriorityType));
            items.AddRange(notCompletedWorkItems.Where(x => !x.WorkItem.IsInProcess));
            items.AddRange(workItems.Where(x => !x.WorkItem.IsInProcess && x.WorkItem.PriorityType == PriorityType.Low && !x.WorkItem.IsNotCompleted));
            return items.Distinct();
        }

        private void ProcessWorkItems(IEnumerable<WorkItemContract> orderedWorkItems)
        {
            foreach (var item in orderedWorkItems)
            {
                Task.Delay(500).Wait();
                var worker = workers.FirstOrDefault(x => x.IsReadyForWork);
                switch (item.WorkItem.PriorityType)
                {
                    case PriorityType.High:
                        if (worker == null)
                        {
                            worker = workers.Any(x => x.CurrentWorkItem?.WorkItem.PriorityType == PriorityType.Low && !x.CurrentWorkItem.WorkItem.IsNotCompleted) ?
                                GetReadyWorker(x => x.CurrentWorkItem?.WorkItem.PriorityType == PriorityType.Low && !x.CurrentWorkItem.WorkItem.IsNotCompleted) :
                                GetReadyWorker(x => x.CurrentWorkItem?.WorkItem.PriorityType == PriorityType.Medium && !x.CurrentWorkItem.WorkItem.IsNotCompleted);
                        }
                        RunTask(worker, item);
                        break;
                    case PriorityType.Medium:
                        worker = worker != null ? worker : GetReadyWorker(x => x.CurrentWorkItem?.WorkItem.PriorityType == PriorityType.Low && !x.CurrentWorkItem.WorkItem.IsNotCompleted);
                        RunTask(worker, item);
                        break;
                    default:
                        RunTask(worker, item);
                        break;
                }
            }
        }

        private Worker GetReadyWorker(Func<Worker, bool> predicate)
        {
            var worker = workers.FirstOrDefault(predicate);
            worker?.CurrentWorkItem.CancellationTokenSource.Cancel();

            return worker;
        }

        private void RunTask(Worker worker, WorkItemContract item)
        {
            if (worker != null)
            {
                worker.IsReadyForWork = false;
                item.WorkItem.IsInProcess = true;
                worker.CurrentWorkItem = item;

                Task.Run(async() => await worker.ProcessWorkItem(item), item.CancellationToken)
                    .ContinueWith(x => RemoveCompletedWorkItem(item));
            }
        }        

        private void InitWorkers()
        {
            for(int i = 0; i < workersCount; i++)
            {
                workers.Add(new Worker(i, excelService));
            }
        }

        private void RemoveCompletedWorkItem(WorkItemContract item)
        {
            if (item.WorkItem.CompletedPersent == completedItemPersent)
            {
                workItems.Remove(item);
                if (notCompletedWorkItems.Any(x => x.WorkItem.Id == item.WorkItem.Id))
                {
                    notCompletedWorkItems.Remove(item);
                }          
            }
            else
            {
                item.WorkItem.IsNotCompleted = true;
                notCompletedWorkItems.Add(new WorkItemContract(item.WorkItem));
                workItems.Remove(item);
            }

            Console.WriteLine($"Completed - {item.WorkItem.CompletedPersent}");
            item.WorkItem.IsInProcess = false;                  
        }
    }
}
