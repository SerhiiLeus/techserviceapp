﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using TechService.ConsoleApp.Domain;
using TechService.ConsoleApp.Models;

namespace TechService.ConsoleApp.Services
{
    public class WorkItemExcelService
    {
        private const string FilePath = @"e:\Development\TechService\BuyingActivities1.xlsx";

        public WorkItemExcelService()
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
        }

        public IEnumerable<WorkItem> GetWorkItemsFormExcel()
        {
            MutexWrapper.Mutex.WaitOne();

            var workItems = new List<WorkItem>();
            byte[] bin = File.ReadAllBytes(FilePath);

            using (MemoryStream stream = new MemoryStream(bin))
            using (ExcelPackage excelPackage = new ExcelPackage(stream))
            {
                foreach (ExcelWorksheet worksheet in excelPackage.Workbook.Worksheets)
                {
                    var startRow = worksheet.Dimension.Start.Row + 1;
                    for (int i = startRow; i <= worksheet.Dimension.End.Row; i++)
                    {
                        var workItemId = worksheet.Cells[i, worksheet.Dimension.Start.Column].Value.ToString();
                        var workItem = new WorkItem(workItemId, WorkItemType.BuyingActivities, PriorityType.High, i);
                        workItems.Add(workItem);
                    }
                }
            }
            MutexWrapper.Mutex.ReleaseMutex();

            return workItems;
        }

        public void RemoveWorkItem(int workerId, int workItemRowNumber)
        {
            Console.WriteLine($"Worker with Id: {workerId} waiting for the file");
            MutexWrapper.Mutex.WaitOne();

            Task.Delay(3000).Wait();

            FileInfo fileInfo = new FileInfo(FilePath);
            using (ExcelPackage excelPackage = new ExcelPackage(fileInfo))
            {
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets[0];
                for (int i = worksheet.Dimension.Start.Column; i <= worksheet.Dimension.End.Column; i++)
                {
                    worksheet.Cells[workItemRowNumber, i].Clear();
                }
                excelPackage.Save();
            }

            Console.WriteLine($"Worker with Id: {workerId} removed worItem");
            MutexWrapper.Mutex.ReleaseMutex();
        }
    }
}
