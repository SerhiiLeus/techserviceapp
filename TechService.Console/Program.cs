﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TechService.ConsoleApp.Domain;
using TechService.ConsoleApp.Services;
using TechService.MessageBroker;

namespace TechService.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            const int workersCount = 5;
            var workService = new WorkService(workersCount);

            using (var messageBus = new MessageBus())
            {
                Task.Run(() => messageBus.SubscribeOnTopic<string>(
                    "WorkItemsCreated",
                    msg => ProcessMessage(msg, workService),
                    CancellationToken.None));
            }
            workService.Work().Wait();

            Console.WriteLine("End");
            Console.ReadLine();
        }

        static void ProcessMessage(string message, WorkService workService)
        {
            var workItems = JsonConvert.DeserializeObject<List<WorkItem>>(message);
            workService.AddWorkItems(workItems);
        }
    }
}
