﻿
using System;

namespace TechService.ConsoleApp.Domain
{
    public class WorkItem
    {
        public string Id { get; set; }

        public WorkItemType WorkItemType { get; set; }

        public PriorityType PriorityType { get; set; }

        public short CompletedPersent { get; set; }

        public bool IsNotCompleted { get; set; }

        public bool IsInProcess { get; set; }

        public int RowIndex { get; private set; }

        public WorkItem() { }

        public WorkItem(string id, WorkItemType workItemType, PriorityType priorityType)
        {
            Id = id;
            WorkItemType = workItemType;
            PriorityType = priorityType;
        }

        public WorkItem(string id, WorkItemType workItemType, PriorityType priorityType, int rowIndex)
            :this(id, workItemType, priorityType)
        {
            RowIndex = rowIndex;
        }
    }
}
