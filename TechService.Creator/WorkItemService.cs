﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechService.ConsoleApp.Domain;
using TechService.MessageBroker;

namespace TechService.Creator
{
    public class WorkItemService
    {
        public async Task CreateDifferentWorkItems()
        {
            var workItems = CreateWorkItems(5, PriorityType.Low);
            workItems.Concat(CreateWorkItems(5, PriorityType.Medium));
            workItems.Concat(CreateWorkItems(5, PriorityType.High));

            await Notify(workItems);
        }

        public async Task CreateLowPriorityWorkItems()
        {
            var workItems = CreateWorkItems(5, PriorityType.Low);
            await Notify(workItems);
        }

        public async Task CreateHighPriorityWorkItems()
        {
            var workItems = CreateWorkItems(5, PriorityType.Medium);

            await Notify(workItems);
        }


        private IEnumerable<WorkItem> CreateWorkItems(int count, PriorityType priorityType)
        {
            var workItems = new List<WorkItem>();
            for (int i = 0; i < count; i++)
            {
                workItems.Add(new WorkItem(Guid.NewGuid().ToString(), WorkItemType.RepairWorks, priorityType));
            }

            return workItems;
        }

        private async Task Notify(IEnumerable<WorkItem> workItems)
        {
            using (var messageBus = new MessageBus())
            {
                await messageBus.SendMessage<IEnumerable<WorkItem>>("WorkItemsCreated", workItems);
            }
        }
    }
}
