﻿using System;
using System.Threading.Tasks;

namespace TechService.Creator
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var workItemService = new WorkItemService();
            string val;

            while (true)
            {
                Console.Write("Press \"a\" to create a bunch of low priority tasks \n");
                Console.Write("Press \"b\" to create a bunch of medium priority tasks \n");
                val = Console.ReadLine();

                switch (val)
                {
                    case "a":
                        await workItemService.CreateLowPriorityWorkItems();
                        break;
                    case "b":
                        await workItemService.CreateHighPriorityWorkItems();
                        break;
                    default:
                        await workItemService.CreateLowPriorityWorkItems();
                        break;
                }
            }
        }
    }
}
