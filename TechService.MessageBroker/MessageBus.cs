﻿using Confluent.Kafka;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace TechService.MessageBroker
{
    public sealed class MessageBus : IDisposable
    {
        private readonly IProducer<Null, string> _producer;
        private IConsumer<Ignore, string> _consumer;

        private readonly ProducerConfig _producerConfig;
        private readonly ConsumerConfig _consumerConfig;

        public MessageBus()
        {
            _producerConfig = new ProducerConfig { BootstrapServers = "localhost:9092" };
            _consumerConfig = new ConsumerConfig
            {
                GroupId = "test-consumer-group",
                BootstrapServers = "localhost:9092",
                AutoOffsetReset = AutoOffsetReset.Earliest
            };

            _producer = new ProducerBuilder<Null, string>(_producerConfig).Build();
        }

        public async Task SendMessage<T>(string topic, T message)
        {
            var serializedMessage = JsonSerializer.Serialize(message);
            var newMessage = new Message<Null, string> { Value = serializedMessage };
            await _producer.ProduceAsync(topic, newMessage);
        }

        public void SubscribeOnTopic<T>(string topic, Action<T> action, CancellationToken cancellationToken) where T : class
        {
            var msgBus = new MessageBus();
            using (msgBus._consumer = new ConsumerBuilder<Ignore, string>(_consumerConfig).Build())
            {
                msgBus._consumer.Assign(new List<TopicPartitionOffset> { new TopicPartitionOffset(topic, 0, -1) });

                while (true)
                {
                    if (cancellationToken.IsCancellationRequested)
                        break;

                    var message = msgBus._consumer.Consume(TimeSpan.FromMilliseconds(10))?.Message;
                    if (message != null)
                    {
                        action(message.Value as T);
                    }
                }
            }
        }

        public void Dispose()
        {
            _producer?.Dispose();
            _consumer?.Dispose();
        }
    }
}
